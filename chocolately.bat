@echo off
SETLOCAL EnableDelayedExpansion
CLS
set currentpath=%cd%
echo "Starting automatic file installation by chocolatey"
echo "Script made by Luukgr"
echo "Run at your own risk"
echo "Script needs to run in admin mode"
ECHO.

:MENU
ECHO ...............................................
ECHO PRESS 1, 2, 3, or 4 to select your task, or 5 to EXIT.
ECHO FIRST TIME HERE? Press 3 to install Chocolatey
ECHO ...............................................
ECHO.
ECHO 1 - Basic apps
ECHO 2 - Developer apps
ECHO 3 - Install/Update Chocolatey
ECHO 4 - Upgrade apps
ECHO 5 - EXIT
ECHO.

SET /P M=Type 1, 2, 3, 4, or 5 then press ENTER:
IF %M%==1 SET CONFIG_FILE=defaultapps.config && GOTO CHECK_APPS
IF %M%==2 SET CONFIG_FILE=devapps.config && GOTO CHECK_APPS
IF %M%==3 GOTO FIR
IF %M%==4 GOTO UPG
IF %M%==5 GOTO EOF

:CHECK_APPS
REM Initialize a flag to indicate if there are apps to uninstall
set "has_uninstallable_apps=0"

REM Dynamically set apps_to_check based on the config file
set apps_to_check=
FOR /F "tokens=*" %%i IN (%currentpath%\%CONFIG_FILE%) DO (
    set apps_to_check=!apps_to_check! "%%i"
)

REM Create empty lists for uninstallable and manually uninstallable apps
set uninstallable_apps=
set manual_uninstall_apps=

REM Check for each application
FOR %%A IN (%apps_to_check%) DO (
    powershell -Command "if (Get-Command -ErrorAction SilentlyContinue %%A) { exit 0 } else { exit 1 }"
    IF ERRORLEVEL 1 (
        set manual_uninstall_apps=!manual_uninstall_apps! %%A
    ) ELSE (
        set uninstallable_apps=!uninstallable_apps! %%A
    )
)




REM Check for each application
FOR %%A IN (%apps_to_check%) DO (
    choco list --local-only | findstr /B /C:%%A >nul
    IF ERRORLEVEL 1 (
        set "manual_uninstall_apps=!manual_uninstall_apps! %%A"
    ) ELSE (
        set "uninstallable_apps=!uninstallable_apps! %%A"
        set "has_uninstallable_apps=1"
    )
)

REM Check if there are any apps to uninstall
IF "!has_uninstallable_apps!"=="0" GOTO INSTALL_APPS

echo Uninstallable apps: !uninstallable_apps!
echo Manually uninstallable apps: !manual_uninstall_apps!
ECHO.

REM Prompt for automatic uninstallation
SET /P auto_uninstall="Attempt to automatically uninstall apps? (Y/N): "
IF /I "!auto_uninstall!"=="Y" GOTO AUTO_UNINSTALL






:INSTALL_APPS
REM Install basic or developer apps based on selection
FOR /F "tokens=*" %%i IN (%currentpath%\%CONFIG_FILE%) DO (
    choco list --local-only | findstr /C:%%i >nul
    IF ERRORLEVEL 1 (
        ECHO Checking if %%i is installed outside Chocolatey...
        REG QUERY HKLM\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall /s /f "%%i" >nul 2>&1
        IF ERRORLEVEL 1 (
            REG QUERY HKLM\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall /s /f "%%i" >nul 2>&1
            IF ERRORLEVEL 1 (
                ECHO Installing %%i...
                choco install %%i --yes
                IF ERRORLEVEL 1 (
                    ECHO Failed to install %%i.
                )
            ) ELSE (
                ECHO %%i is already installed outside Chocolatey. Skipping installation.
            )
        ) ELSE (
            ECHO %%i is already installed outside Chocolatey. Skipping installation.
        )
    ) ELSE (
        ECHO %%i is already installed by Chocolatey.
    )
)
ECHO ...............................................
ECHO Installation process completed.
ECHO ...............................................
GOTO MENU
...





:FIR
@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "if((Get-Command choco -ErrorAction SilentlyContinue) -eq $null) { iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')) } else { choco upgrade chocolatey }"
choco feature enable -n allowGlobalConfirmation
choco upgrade chocolatey
ECHO ...............................................
ECHO A RESTART OF THE BATCH FILE IS MAYBE NECESSARY!!
ECHO ...............................................
GOTO MENU

:UPG
choco upgrade all --yes
ECHO ...............................................
ECHO All apps upgraded successfully.
ECHO ...............................................
GOTO MENU

:EOF
ECHO Exiting script.
EXIT
