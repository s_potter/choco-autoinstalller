
# Incase of hash mismatch see:
https://stackoverflow.com/questions/69140740/checksum-error-in-installing-software-using-chocolatey

I would recommend the --checksum command line argument, way safer than --ignore-checksums

This requires a bit little extra work compared to just ignoring. You have three options how to get the correct checksum, safest a), then b) then least safe is c)

a) go to the original source of download, (I mean, where the package author also intend to download the installable) and if there is a checksum information use that.

b) go to the original source of download, download yourself, then check it with virustotal. This servers two purpose: you got a trust/no trust picture, and secondly, the hash what virustotal calculates is exactly what choco expects. So if you decide to trust in the downloaded stuff based on virustotal, then you can instantly use that virustotal hash in your --checksum command line argument.

c) use the checksum in the choco install error message

https://codesigningstore.com/how-to-check-file-checksum

e.g.

```
cd "D:\Downloads"
certUtil –hashfile BRU_setup_3.4.4.0.exe SHA256
choco install bulkrenameutility --checksum 6359d8f55feeb92648b15fffa44bed2994ff1daa3d9d38ab45ad00799050f3d6
```
